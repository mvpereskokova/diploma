from itertools import cycle
from ruptures.utils import draw_bkps
import numpy as np
import scipy.stats as ss


class Mixture(ss.rv_continuous):
    def __init__(self, submodels, probs):
        super().__init__()
        self.submodels = submodels
        self.probs = probs

    def _pdf(self, x):
        return sum(
            [
                self.submodels[i].pdf(x) * self.probs[i]
                for i in range(len(self.submodels))
            ]
        )

    def _cdf(self, x):
        return sum(
            [
                self.submodels[i].cdf(x) * self.probs[i]
                for i in range(len(self.submodels))
            ]
        )

    def rvs(self, size):
        submodel_choices = np.random.choice(
            len(self.submodels), size=size, p=self.probs
        )
        submodel_samples = [submodel.rvs(size=size) for submodel in self.submodels]
        rvs = np.choose(submodel_choices, submodel_samples)
        return rvs


class Noisy(ss.rv_continuous):
    def __init__(self, model, noise, prob=0.5):
        super().__init__()
        self.model = model
        self.noise = noise
        self.prob = prob

    def rvs(self, size):
        choices = np.random.choice(2, size=size, p=[1 - self.prob, self.prob])
        return self.model.rvs(size=size) + abs(self.noise.rvs(size=size) * choices)


def pw_normal_intervals(
    n_samples=200,
    n_bkps=3,
    interval_len=40,
    d1=ss.norm(0, 1),
    d2=ss.norm(0, 2),
    seed=None,
):
    # breakpoints
    bkps_first = draw_bkps(n_samples, n_bkps, seed=seed)
    bkps = np.array(
        [(bkps_first[i], bkps_first[i] + interval_len) for i in range(n_bkps)]
    ).flatten()
    bkps = np.hstack((bkps, [n_samples]))
    # we create the signal
    signal = np.zeros(n_samples, dtype=float)
    rng = np.random.default_rng(seed=seed)
    for sub, d in zip(np.split(signal, bkps), cycle((d1, 0, d2, -1))):
        n_sub = sub.shape[0]
        if d == 0:
            choice = [
                rng.choice(2, 1, p=[1 - i / n_sub, i / n_sub])[0] for i in range(n_sub)
            ]
            sub += d2.rvs(n_sub) * choice + d1.rvs(n_sub) * (np.ones(n_sub) - choice)
        elif d == -1:
            choice = [
                rng.choice(2, 1, p=[i / n_sub, 1 - i / n_sub])[0] for i in range(n_sub)
            ]
            sub += d2.rvs(n_sub) * choice + d1.rvs(n_sub) * (np.ones(n_sub) - choice)
        else:
            sub += d.rvs(n_sub)

    return signal, bkps
