import pandas as pd
from scipy.spatial.distance import cdist
from scipy.spatial import distance
import numpy as np

from ruptures.base import BaseEstimator

FPScore = 5_000
FNScore = 500
DistThreshold = 50


# compare pairs score
def partialPairsScore(realBkps, foundBkps):
    realInts = []
    foundInts = []
    for i in range(0, len(realBkps), 2):
        realInts.append(tuple([realBkps[i], realBkps[i + 1]]))
    for i in range(0, len(foundBkps), 2):
        foundInts.append(tuple([foundBkps[i], foundBkps[i + 1]]))

    score = 0

    counts = dict()
    for found in foundInts:
        closest = None
        for real in realInts:
            if closest is None or distance.cityblock(real, found) < distance.cityblock(
                closest, found
            ):
                closest = real
        if closest is None or distance.cityblock(closest, found) > DistThreshold:
            score += FPScore
        else:
            if not closest in counts:
                counts[closest] = 0
            counts[closest] += 1
            score += distance.cityblock(closest, found)
    for real in realInts:
        if not real in counts:
            score += FNScore

    return score


NotFoundScore = 1000


# hausdorf
def partialScore(realBkps, foundBkps):
    if len(realBkps) == 0 or len(foundBkps) == 0:
        return NotFoundScore
    bkps1_arr = [[realBkps[i], realBkps[i + 1]] for i in range(0, len(realBkps) - 1, 2)]
    bkps2_arr = [
        [foundBkps[i], foundBkps[i + 1]] for i in range(0, len(foundBkps) - 1, 2)
    ]
    pw_dist = cdist(bkps1_arr, bkps2_arr, "cityblock")
    res = max(pw_dist.min(axis=0).max(), pw_dist.min(axis=1).max())

    return res


# randIndex
def partialScoreRandindex(realBkps, foundBkps, n):
    bkps1 = list(realBkps) + [n]
    bkps2 = foundBkps + [n]

    bkps1_with_0 = [0] + bkps1
    bkps2_with_0 = [0] + bkps2
    n_bkps1 = len(bkps1)
    n_bkps2 = len(bkps2)

    disagreement = 0
    beginj: int = 0  # avoids unnecessary computations
    for index_bkps1 in range(n_bkps1):
        start1: int = bkps1_with_0[index_bkps1]
        end1: int = bkps1_with_0[index_bkps1 + 1]
        for index_bkps2 in range(beginj, n_bkps2):
            start2: int = bkps2_with_0[index_bkps2]
            end2: int = bkps2_with_0[index_bkps2 + 1]
            nij = max(min(end1, end2) - max(start1, start2), 0)
            disagreement += nij * abs(end1 - end2)

            # we can skip the rest of the iteration, nij will be 0
            if end1 < end2:
                break
            else:
                beginj = index_bkps2 + 1

    disagreement /= n * (n - 1) / 2
    return 1.0 - disagreement


def countScore(estimator: BaseEstimator, df: pd.DataFrame):
    df = df.reset_index()
    result = dict()
    allresults = []

    for _, row in df.iterrows():
        signal = np.array(list(map(float, row["signal"][1:-1].split())))
        bkps = np.array(list(map(float, row["bkps"][1:-1].split()))[:-1])
        name = row["tag"]

        if not name in result:
            result[name] = []

        algo = estimator.fit(signal)
        current = algo.predict()

        score = partialPairsScore(bkps, current)

        result[name].append(score)
        allresults.append(score)

    scores = dict()
    for key in result:
        scores[key] = np.median(result[key], axis=0)
    scores["All"] = np.median(allresults, axis=0)
    return scores
