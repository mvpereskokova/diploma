import numpy as np
import scipy.stats as ss

from ruptures.base import BaseCost, BaseEstimator
from ruptures.costs import cost_factory

from .Union import Union


class WindowCost(BaseEstimator):
    def __init__(
        self, width=100, model="l2", custom_cost=None, params=None, epsilon=100
    ):
        self.width = 2 * (width // 2)
        self.n_samples = None
        self.signal = None
        self.inds = None
        self.model = model
        if custom_cost is not None and isinstance(custom_cost, BaseCost):
            self.cost = custom_cost
        else:
            if params is None:
                self.cost = cost_factory(model=model)
            else:
                self.cost = cost_factory(model=model, **params)
        self.value = list()
        self.epsilon = epsilon
        self.algoName = "WindowCost"

    def name(self):
        return self.algoName

    def setName(self, name):
        self.algoName = name

    def _seg(self):
        bkps = [self.n_samples]
        peak_inds = np.argsort(self.value)
        while len(peak_inds) > 0:
            peak = peak_inds[-1]
            peak_inds = peak_inds[:-1]
            if self.value[peak] < self.epsilon:
                break
            bkps.append(self.inds[peak])
        bkps.sort()

        return Union(bkps[:-1])

    def fit(self, signal) -> "WindowCost":
        self.cost.fit(signal)
        if signal.ndim == 1:
            self.signal = signal.reshape(-1, 1)
        else:
            self.signal = signal

        self.n_samples, _ = self.signal.shape
        self.inds = np.arange(self.n_samples, step=1)
        keep = (self.inds >= self.width // 2) & (
            self.inds < self.n_samples - self.width // 2
        )
        self.inds = self.inds[keep]

        # compute score
        score = list()
        for k in self.inds:
            start, mid, end = k - self.width // 2, k, k + self.width // 2
            value = (
                self.cost.error(start, end)
                - self.cost.error(start, mid)
                - self.cost.error(mid, end)
            )
            if self.model == "rank":
                score.append(value[0][0])
            else:
                score.append(value)

        self.value = np.array(score)
        return self

    def predict(self):
        bkps = self._seg()
        return bkps

    def fit_predict(self, signal):
        self.fit(signal)
        return self.predict()
