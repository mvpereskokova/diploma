import numpy as np
from ruptures.base import BaseCost, BaseEstimator
from ruptures.costs import cost_factory
from ruptures.exceptions import BadSegmentationParameters
from ruptures.utils import pairwise, sanity_check
from .Union import Union


class BinsegCost(BaseEstimator):
    def __init__(
        self,
        model="l2",
        custom_cost=None,
        min_size=2,
        params=None,
        pen=None,
        epsilon=None,
    ):
        if custom_cost is not None and isinstance(custom_cost, BaseCost):
            self.cost = custom_cost
        else:
            if params is None:
                self.cost = cost_factory(model=model)
            else:
                self.cost = cost_factory(model=model, **params)
        assert pen != None or epsilon != None
        self.min_size = max(min_size, self.cost.min_size)
        self.n_samples = None
        self.signal = None
        self.pen = pen
        self.epsilon = epsilon
        self.algoName = "BinsegCost"

    def name(self):
        return self.algoName

    def setName(self, name):
        self.algoName = name

    def _seg(self):
        bkps = [self.n_samples]
        stop = False
        while not stop:
            stop = True
            new_bkps = [
                self.single_bkp(start, end) for start, end in pairwise([0] + bkps)
            ]
            if not new_bkps:
                break

            bkp, gain = max(new_bkps, key=lambda x: x[1])
            if self.pen is not None:
                if gain > self.pen:
                    stop = False
            elif self.epsilon is not None:
                error = self.cost.sum_of_costs(bkps)
                if error > self.epsilon:
                    stop = False

            if not stop:
                bkps.append(bkp)
                bkps.sort()
        partition = {
            (start, end): self.cost.error(start, end)
            for start, end in pairwise([0] + bkps)
        }
        return partition

    def single_bkp(self, start, end):
        segment_cost = self.cost.error(start, end)
        if np.isinf(segment_cost) and segment_cost < 0:  # if cost is -inf
            return None, 0
        gain_list = list()
        for bkp in range(start, end):
            if bkp - start >= self.min_size and end - bkp >= self.min_size:
                gain = (
                    segment_cost
                    - self.cost.error(start, bkp)
                    - self.cost.error(bkp, end)
                )
                gain_list.append((gain, bkp))

        if not gain_list:
            return None, 0

        gain, bkp = max(gain_list)
        return bkp, gain

    def fit(self, signal) -> "BinsegCost":
        if signal.ndim == 1:
            self.signal = signal.reshape(-1, 1)
        else:
            self.signal = signal
        self.n_samples, _ = self.signal.shape
        self.cost.fit(signal)

        return self

    def predict(self):
        # raise an exception in case of impossible segmentation configuration
        if self.min_size > self.cost.signal.shape[0]:
            raise BadSegmentationParameters

        partition = self._seg()
        bkps = sorted(e for _, e in partition.keys())
        return Union(bkps[:-1])

    def fit_predict(self, signal):
        self.fit(signal)
        return self.predict()
